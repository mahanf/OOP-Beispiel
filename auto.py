class Auto:
    def __init__(self, farbe, versicherung):
        self.__maxGeschwindigkeit = 180
        self.__farbe = farbe
        self.__fahrer = None
        self.__versicherung = versicherung

    def setFarbe(self, neueFarbe):
        self.__farbe = neueFarbe

    def getFarbe(self):
        return self.__farbe

    def getFahrer(self):
        return self.__fahrer

    def getVersicherung(self):
        return self.__versicherung

    def leihen(self, fahrer):
        self.__fahrer = fahrer
